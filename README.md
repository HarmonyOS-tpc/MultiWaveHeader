该三方开源库从github fork过来，主要将底层接口调用的实现修改成鸿蒙接口的实现，将三方库鸿蒙化，供开发鸿蒙应用的开发者使用

fork地址：https://github.com/scwang90/MultiWaveHeader

fork版本号/日期：release / 2020/6/7


# MultiWaveHeader

MultiWaveHeader 是一个可以高度定制每个波形的水波控件。

原项目Readme地址：https://github.com/scwang90/MultiWaveHeader/blob/release/README.md

项目移植状态：支持组件所有基本功能

完成度：100%

调用差异：无





## 导入方法

**har导入**

将har包放入lib文件夹并在build.gradle添加

```
implementation fileTree(dir: 'libs', include: ['*.har'])
```

**Library引用**

添加本工程中的模块到任意工程中，在需要使用的模块build.gradle中添加

```
compile project(path: ':library')
```

or

```
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:MultiWaveHeader:1.0.1'
```
注意：

在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下
## 使用方法

##### XML
```xml
    <com.scwang.wave.MultiWaveHeader
            ohos:id="$+id:multiWaveHeader"
            ohos:height="match_parent"
            ohos:width="match_parent"
            ohos:mwhEnableFullScreen="true"
            ohos:mwhProgress="0.45">
    </com.scwang.wave.MultiWaveHeader>
```

##### Java

```java
//初始化
multiWaveHeader = (MultiWaveHeader) findComponentById(ResourceTable.Id_multiWaveHeader);
```

## 一些功能设置介绍
```
//设置整个水波纹的形状
multiWaveHeader.setShape();
//支持设置水波的数量
multiWaveHeader.setWaves();
//设置水波纹的振幅
multiWaveHeader.setWaveHeight();
//设置水波纹的高度
multiWaveHeader.setProgress();
//设置水波纹的速度
multiWaveHeader.setVelocity();
//设置水波纹颜色渐变方向
multiWaveHeader.setGradientAngle();
//设置水波纹的透明度
multiWaveHeader.setColorAlpha();
//设置水波纹开始的渐变颜色
multiWaveHeader.setStartColor();
//设置水波纹结束的渐变颜色
multiWaveHeader.setCloseColor();
//开始水波纹动画
multiWaveHeader.start();
//停止水波纹动画
multiWaveHeader.stop();
//获取整个水波纹的形状
multiWaveHeader.getShape();
//获取水波的振幅
multiWaveHeader.getWaveHeight();
//获取水波的高度
multiWaveHeader.getProgress();
//获取水波纹的速度
multiWaveHeader.getVelocity();
//获取水波纹颜色渐变方向
multiWaveHeader.getGradientAngle();
//获取水波纹的透明度
multiWaveHeader.getColorAlpha();
//获取水波纹开始的渐变颜色
multiWaveHeader.getStartColor();
//获取水波纹结束的渐变颜色
multiWaveHeader.getCloseColor();
//获取水波纹是否正在运行
multiWaveHeader.isRunning();
```

#### 可以高度定制每一条水波.

###### java
```java
    MultiWaveHeader waveHeader = findComponentById(R.id.waveHeader);

    String[] waves = new String[]{
        "70,25,1.4,1.4,-26",//wave-1:offsetX(dp),offsetY(dp),scaleX,scaleY,velocity(dp/s)
        "100,5,1.4,1.2,15",
        "420,0,1.15,1,-10",//wave-3:水平偏移(dp),竖直偏移(dp),水平拉伸,竖直拉伸,速度(dp/s)
        "520,10,1.7,1.5,20",
        "220,0,1,1,-15",
    };
    waveHeader.setWaves(String.join(" ", Arrays.asList(waves)));// custom
    waveHeader.setWaves("PairWave");// default two waves
    waveHeader.setWaves("MultiWave");// default five waves

```

###### xml
```xml
    <com.scwang.wave.MultiWaveHeader
        ohos:id="$+id:waveHeader"
        ohos:height="match_parent"
        ohos:width="match_parent"
        ohos:mwhWaves="PairWave"
        ohos:mwhWaves="MultiWave"
        ohos:mwhWaves="
            70,25,1.4,1.4,-26
            100,5,1.4,1.2,15
            420,0,1.15,1,-10
            520,10,1.7,1.5,20
            220,0,1,1,-15"/>
```
注意：mwhWaves的值要设置对，不然组件会渲染不成功     

License
--
```Copyright 2017 scwang90

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```


