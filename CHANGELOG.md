1.0.0

Initial Release

Support Features:
--------------------------------------------
1) 设置整个水波纹的形状
multiWaveHeader.setShape();
1) 设置水波的数量
multiWaveHeader.setWaves();
1) 设置水波纹的振幅
multiWaveHeader.setWaveHeight();
1) 设置水波纹的高度
multiWaveHeader.setProgress();
1) 设置水波纹的速度
multiWaveHeader.setVelocity();
1) 设置水波纹颜色渐变方向
multiWaveHeader.setGradientAngle();
1) 设置水波纹的透明度
multiWaveHeader.setColorAlpha();
1) 设置水波纹开始的渐变颜色
multiWaveHeader.setStartColor();
1) 设置水波纹结束的渐变颜色
multiWaveHeader.setCloseColor();
1) 开始水波纹动画
multiWaveHeader.start();
1) 停止水波纹动画
multiWaveHeader.stop();
1) 获取整个水波纹的形状
multiWaveHeader.getShape();
1) 获取水波的振幅
multiWaveHeader.getWaveHeight();
1) 获取水波的高度
multiWaveHeader.getProgress();
1) 获取水波纹的速度
multiWaveHeader.getVelocity();
1) 获取水波纹颜色渐变方向
multiWaveHeader.getGradientAngle();
1) 获取水波纹的透明度
multiWaveHeader.getColorAlpha();
1) 获取水波纹开始的渐变颜色
multiWaveHeader.getStartColor();
1) 获取水波纹结束的渐变颜色
multiWaveHeader.getCloseColor();
1) 获取水波纹是否正在运行
multiWaveHeader.isRunning();

Not Support Features:
--------------------------------------------------------

无