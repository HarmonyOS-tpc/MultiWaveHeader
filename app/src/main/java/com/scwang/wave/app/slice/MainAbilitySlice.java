/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.scwang.wave.app.slice;

import com.scwang.wave.LogUtil;
import com.scwang.wave.MultiWaveHeader;
import com.scwang.wave.ShapeType;
import com.scwang.wave.app.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.AbsButton;
import ohos.agp.components.Checkbox;
import ohos.agp.components.RadioContainer;
import ohos.agp.components.Slider;
import ohos.agp.utils.Color;

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Main ability slice
 */
public class MainAbilitySlice extends AbilitySlice implements Slider.ValueChangedListener {
    /**
     * The constant Multi wave header
     */
    private MultiWaveHeader multiWaveHeader;
    /**
     * The constant Cb up side down
     */
    private Checkbox cbUpSideDown;
    /**
     * The constant Cb is running
     */
    private Checkbox cbIsRunning;
    /**
     * The constant Radio container
     */
    private RadioContainer radioContainer;

    /**
     * The constant Slider number
     */
    private Slider sliderNumber;
    /**
     * The constant Slider wave
     */
    private Slider sliderWave;
    /**
     * The constant Slider angle
     */
    private Slider sliderAngle;
    /**
     * The constant Slider alpha
     */
    private Slider sliderAlpha;
    /**
     * The constant Slider velocity
     */
    private Slider sliderVelocity;
    /**
     * The constant Slider progress
     */
    private Slider sliderProgress;
    /**
     * The constant Slider start color
     */
    private Slider sliderStartColor;
    /**
     * The constant Slider close color
     */
    private Slider sliderCloseColor;

    private String[] stringColors=new String[]{"green","cyan","yellow","red","magenta","blue"};

    /**
     * On start *
     *
     * @param intent intent
     */
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        multiWaveHeader = (MultiWaveHeader) findComponentById(ResourceTable.Id_multiWaveHeader);
        radioContainer = (RadioContainer) findComponentById(ResourceTable.Id_radio_container);
        cbUpSideDown = (Checkbox) findComponentById(ResourceTable.Id_cb_up_side_down);
        cbIsRunning = (Checkbox) findComponentById(ResourceTable.Id_cb_is_running);
        sliderNumber = (Slider) findComponentById(ResourceTable.Id_slider_number);
        sliderWave = (Slider) findComponentById(ResourceTable.Id_slider_wave);
        sliderAngle = (Slider) findComponentById(ResourceTable.Id_slider_angle);
        sliderAlpha = (Slider) findComponentById(ResourceTable.Id_slider_alpha);
        sliderVelocity = (Slider) findComponentById(ResourceTable.Id_slider_velocity);
        sliderProgress = (Slider) findComponentById(ResourceTable.Id_slider_progress);
        sliderStartColor = (Slider) findComponentById(ResourceTable.Id_slider_start_color);
        sliderCloseColor = (Slider) findComponentById(ResourceTable.Id_slider_close_color);
        sliderNumber.setValueChangedListener(this);
        sliderWave.setValueChangedListener(this);
        sliderAngle.setValueChangedListener(this);
        sliderAlpha.setValueChangedListener(this);
        sliderVelocity.setValueChangedListener(this);
        sliderProgress.setValueChangedListener(this);
        sliderStartColor.setValueChangedListener(this);
        sliderCloseColor.setValueChangedListener(this);
        cbUpSideDown.setCheckedStateChangedListener(this::onCheckedChanged);
        cbIsRunning.setCheckedStateChangedListener(this::onCheckedChanged);
        radioContainer.setMarkChangedListener(this::onCheckedChanged);
        cbIsRunning.setChecked(multiWaveHeader.isRunning());
        cbUpSideDown.setChecked(multiWaveHeader.getScaleY() == -1f);

        sliderNumber.setProgressValue(5);
        sliderAngle.setProgressValue(multiWaveHeader.getGradientAngle());
        sliderVelocity.setProgressValue((int) (multiWaveHeader.getVelocity() * 10));
        sliderAlpha.setProgressValue((int) (multiWaveHeader.getColorAlpha() * 100));
        sliderProgress.setProgressValue((int) (multiWaveHeader.getProgress() * 100));
        sliderWave.setProgressValue(multiWaveHeader.getWaveHeight() / (getResourceManager().getDeviceCapability().screenDensity / 160));

        sliderStartColor.setProgressColors(new int[]{Color.GREEN.getValue(),Color.CYAN.getValue(),Color.YELLOW.getValue(),Color.RED.getValue(),Color.MAGENTA.getValue(),Color.BLUE.getValue()});
        sliderCloseColor.setProgressColors(new int[]{Color.GREEN.getValue(),Color.CYAN.getValue(),Color.YELLOW.getValue(),Color.RED.getValue(),Color.MAGENTA.getValue(),Color.BLUE.getValue()});
        radioContainer.mark(multiWaveHeader.getShape().ordinal());
    }

    /**
     * On checked changed *
     *
     * @param radioContainer radio container
     * @param checkedId      checked id
     */
    private void onCheckedChanged(RadioContainer radioContainer, int checkedId) {
        switch (checkedId) {
            case 0:
                multiWaveHeader.setShape(ShapeType.Rect);
                break;
            case 1:
                multiWaveHeader.setShape(ShapeType.RoundRect);
                break;
            case 2:
                multiWaveHeader.setShape(ShapeType.Oval);
                break;
        }
    }

    /**
     * On checked changed *
     *
     * @param absButton abs button
     * @param isChecked is checked
     */
    private void onCheckedChanged(AbsButton absButton, boolean isChecked) {

        switch (absButton.getId()) {
            case ResourceTable.Id_cb_up_side_down:
                multiWaveHeader.setRotation(isChecked ? 180f : 0);
                break;
            case ResourceTable.Id_cb_is_running:
                if (isChecked) {
                    multiWaveHeader.start();
                } else {
                    multiWaveHeader.stop();
                }
                break;
        }
    }

    /**
     * On active
     */
    @Override
    public void onActive() {
        super.onActive();
    }

    /**
     * On foreground *
     *
     * @param intent intent
     */
    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    /**
     * On progress updated *
     *
     * @param slider   slider
     * @param progress progress
     * @param fromUser from user
     */
    @Override
    public void onProgressUpdated(Slider slider, int progress, boolean fromUser) {
        if (!fromUser){
            return;
        }
        slider.setProgressHintText("" + progress);
        slider.setProgressHintTextColor(new Color(Color.getIntColor("#FFCC99")));

        switch (slider.getId()) {
            case ResourceTable.Id_slider_wave:
                multiWaveHeader.setWaveHeight(progress);
                break;
            case ResourceTable.Id_slider_angle:
                multiWaveHeader.setGradientAngle(progress);
                break;
            case ResourceTable.Id_slider_alpha:
                multiWaveHeader.setColorAlpha(1f * progress / 100);
                break;
            case ResourceTable.Id_slider_velocity:
                multiWaveHeader.setVelocity(1f * progress / 10);
                break;
            case ResourceTable.Id_slider_start_color:
                multiWaveHeader.setStartColor(slider.getProgressColors()[progress]);
                slider.setProgressHintText(stringColors[progress]);
                break;
            case ResourceTable.Id_slider_close_color:
                multiWaveHeader.setCloseColor(slider.getProgressColors()[progress]);
                slider.setProgressHintText(stringColors[progress]);
                break;
        }
    }

    /**
     * On touch start *
     *
     * @param slider slider
     */
    @Override
    public void onTouchStart(Slider slider) {

    }

    /**
     * On touch end *
     *
     * @param slider slider
     */
    @Override
    public void onTouchEnd(Slider slider) {
        int progress = slider.getProgress();
        LogUtil.info("onProgressUpdated", "onProgressUpdated: " + slider.getId() + "  " + ResourceTable.Id_slider_number);
        switch (slider.getId()) {
            case ResourceTable.Id_slider_number:
                if (progress == 2) {
                    multiWaveHeader.setWaves("0,0,1,1,25\n90,0,1,1,25");
                } else {
                    List waves = Arrays.asList("70,25,1.4,1.4,-26\n100,5,1.4,1.2,15\n420,0,1.15,1,-10\n520,10,1.7,1.5,20\n220,0,1,1,-15".split("\n"));
                    List list = waves.subList(0, progress);
                    String collect = (String) list.stream().collect(Collectors.joining(", "));
                    multiWaveHeader.setWaves(collect);
                }
                break;
            case ResourceTable.Id_slider_progress:
                multiWaveHeader.setProgress(1f * progress / 100);
                break;

        }
    }
}
