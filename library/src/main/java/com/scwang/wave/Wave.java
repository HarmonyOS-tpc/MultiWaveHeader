package com.scwang.wave;

import ohos.agp.render.Path;
import ohos.app.Context;

/**
 * 水波对象
 * Created by SCWANG on 2017/12/11.
 */
class Wave {

    /**
     * The constant Path水波路径
     */
    Path path;
    /**
     * The constant Width画布宽度（2倍波长）
     */
    int width;
    /**
     * The constant Wave波幅（振幅）
     */
    int wave;
    /**
     * The constant Offset x水波的水平偏移量
     */
    float offsetX;
    /**
     * The constant Offset y水波的竖直偏移量
     */
    float offsetY;
    /**
     * The constant Velocity水波移动速度（像素/秒）
     */
    float velocity;
    /**
     * The constant Scale x水平拉伸比例
     */
    private float scaleX;
    /**
     * The constant Scale y竖直拉伸比例
     */
    private float scaleY;
    /**
     * The constant Cur wave
     */
    private int curWave;

    /**
     * 通过参数构造一个水波对象
     *
     * @param offsetX  水平偏移量
     * @param offsetY  竖直偏移量
     * @param velocity 移动速度（像素/秒）
     * @param scaleX   水平拉伸量
     * @param scaleY   竖直拉伸量
     * @param wave     波幅（波宽度）
     */

    Wave(int offsetX, int offsetY, int velocity, float scaleX, float scaleY, int wave) {
        this.wave = wave; // 波幅（波宽）
        this.scaleX = scaleX; // 水平拉伸量
        this.scaleY = scaleY; // 竖直拉伸量
        this.offsetX = offsetX; // 水平偏移量
        this.offsetY = offsetY; // 竖直偏移量
        this.velocity = velocity; // 移动速度（像素/秒）
        this.path = new Path();
    }

    /**
     * Update wave path *
     *
     * @param context      context
     * @param width          width
     * @param height          height
     * @param waveHeight wave height
     * @param fullScreen full screen
     * @param progress   progress
     */
    protected void updateWavePath(Context context,int width, int height, int waveHeight, boolean fullScreen, float progress) {
        this.wave = waveHeight;
        this.width = (int) (2 * scaleX * width);  // 画布宽度（2倍波长）
        this.path = buildWavePath(context,this.width, height, fullScreen, progress);
    }

    /**
     * Update wave path *
     *
     * @param context      context
     * @param width        width
     * @param height        height
     * @param progress progress
     */
    protected void updateWavePath(Context context,int width, int height, float progress) {
        int waveHeight = (int) (scaleY * this.wave); // 计算拉伸之后的波幅
        float maxWave = height * Math.max(0, (1 - progress));
        if (waveHeight > maxWave) {
            waveHeight = (int) maxWave;
        }

        if (curWave != waveHeight) {
            this.width = (int) (2 * scaleX * width);  // 画布宽度（2倍波长）
            this.path = buildWavePath(context,this.width, height, true, progress);
        }
    }

    /**
     * Build wave path path
     *
     * @param context      context
     * @param width      width
     * @param height     height
     * @param fullScreen full screen
     * @param progress   progress
     * @return the path
     */
    protected Path buildWavePath(Context context,int width, int height, boolean fullScreen, float progress) {
        int dp = Util.dp2px(context,1); // 一个dp在当前设备表示的像素量（水波的绘制精度设为一个dp单位）
        if (dp < 1) {
            dp = 1;
        }

        int waveHeight = (int) (scaleY * this.wave); // 计算拉伸之后的波幅

        if (fullScreen) {
            float maxWave = height * Math.max(0, (1 - progress));
            if (waveHeight > maxWave) {
                waveHeight = (int) maxWave;
            }
        }
        this.curWave = waveHeight;
        path.reset();
        path.moveTo(0, 0);
        path.lineTo(0, height - waveHeight);

        if (waveHeight > 0) {
            for (int x = dp; x < width; x += dp) {
                path.lineTo(x, height - waveHeight - waveHeight * (float) Math.sin(4.0 * Math.PI * x / width));
            }
        }

        path.lineTo(width, height - waveHeight);
        path.lineTo(width, 0);
        path.close();
        return path;
    }
}