/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.scwang.wave;

import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;

/**
 * Attr utils
 */
public class AttrUtils {
    /**
     * Get int from attr int
     *
     * @param attrs        attrs
     * @param name         name
     * @param defaultValue default value
     * @return the int
     */
    public static int getIntFromAttr(AttrSet attrs, String name, int defaultValue) {
        int value = defaultValue;
        try {
            if (attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null) {
                value = attrs.getAttr(name).get().getIntegerValue();
            }
        } catch (Exception e) {
            LogUtil.error(AttrUtils.class.getName(), e.getMessage());
        }
        return value;
    }

    /**
     * Get float from attr float
     *
     * @param attrs        attrs
     * @param name         name
     * @param defaultValue default value
     * @return the float
     */
    public static float getFloatFromAttr(AttrSet attrs, String name, float defaultValue) {
        float value = defaultValue;
        try {
            if (attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null) {
                value = attrs.getAttr(name).get().getFloatValue();
            }
        } catch (Exception e) {
            LogUtil.error(AttrUtils.class.getName(), e.getMessage());
        }
        return value;
    }

    /**
     * Get boolean from attr boolean
     *
     * @param attrs        attrs
     * @param name         name
     * @param defaultValue default value
     * @return the boolean
     */
    public static boolean getBooleanFromAttr(AttrSet attrs, String name, boolean defaultValue) {
        boolean value = defaultValue;
        try {
            if (attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null) {
                value = attrs.getAttr(name).get().getBoolValue();
            }
        } catch (Exception e) {
            LogUtil.error(AttrUtils.class.getName(), e.getMessage());
        }
        return value;
    }

    /**
     * Get long from attr long
     *
     * @param attrs        attrs
     * @param name         name
     * @param defaultValue default value
     * @return the long
     */
    public static long getLongFromAttr(AttrSet attrs, String name, long defaultValue) {
        long value = defaultValue;
        try {
            if (attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null) {
                value = attrs.getAttr(name).get().getLongValue();
            }
        } catch (Exception e) {
            LogUtil.error(AttrUtils.class.getName(), e.getMessage());
        }
        return value;
    }

    /**
     * Get color from attr int
     *
     * @param attrs        attrs
     * @param name         name
     * @param defaultValue default value
     * @return the int
     */
    public static int getColorFromAttr(AttrSet attrs, String name, int defaultValue) {
        int value = defaultValue;
        try {
            if (attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null) {
                value = attrs.getAttr(name).get().getColorValue().getValue();
            }
        } catch (Exception e) {
            LogUtil.error(AttrUtils.class.getName(), e.getMessage());
        }
        return value;
    }

    /**
     * Get element from attr element
     *
     * @param attrs        attrs
     * @param name         name
     * @param defaultValue default value
     * @return the element
     */
    public static Element getElementFromAttr(AttrSet attrs, String name, Element defaultValue) {
        Element value = defaultValue;
        try {
            if (attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null) {
                value = attrs.getAttr(name).get().getElement();
            }
        } catch (Exception e) {
            LogUtil.error(AttrUtils.class.getName(), e.getMessage());
        }
        return value;
    }

    /**
     * Get dimension from attr int
     *
     * @param attrs        attrs
     * @param name         name
     * @param defaultValue default value
     * @return the int
     */
    public static int getDimensionFromAttr(AttrSet attrs, String name, int defaultValue) {
        int value = defaultValue;
        try {
            if (attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null) {
                value = attrs.getAttr(name).get().getDimensionValue();
            }
        } catch (Exception e) {
            LogUtil.error(AttrUtils.class.getName(), e.getMessage());
        }
        return value;
    }

    /**
     * Get string from attr string
     *
     * @param attrs        attrs
     * @param name         name
     * @param defaultValue default value
     * @return the string
     */
    public static String getStringFromAttr(AttrSet attrs, String name, String defaultValue) {
        String value = defaultValue;
        try {
            if (attrs.getAttr(name).isPresent() && attrs.getAttr(name).get() != null) {
                value = attrs.getAttr(name).get().getStringValue();
            }
        } catch (Exception e) {
            LogUtil.error(AttrUtils.class.getName(), e.getMessage());
        }
        return value;
    }
}
