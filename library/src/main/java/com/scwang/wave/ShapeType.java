package com.scwang.wave;

/**
 * Shape type
 */
public enum ShapeType {
    /**
     * Rect shape type
     */
    Rect,
    /**
     * Round rect shape type
     */
    RoundRect,
    /**
     * Oval shape type
     */
    Oval;

    /**
     * getShapeType
     *
     * @param index index
     * @return ShapeType
     */
    public static ShapeType getShapeType(int index) {
        if (index >= ShapeType.values().length) {
            return ShapeType.values()[0];
        }
        return ShapeType.values()[index];
    }
}