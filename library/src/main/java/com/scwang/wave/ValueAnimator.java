/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.scwang.wave;

import ohos.agp.animation.AnimatorValue;


/**
 * 可以设置初始值和结束值的数值动画
 */
public class ValueAnimator extends AnimatorValue {

    /**
     * The constant Start
     */
    private float start = 0;
    /**
     * The constant End
     */
    private float end = 1;
    /**
     * The constant My value update listener
     */
    private ValueUpdateListener myValueUpdateListener;
    /**
     * The constant Value update listener
     */
    ValueUpdateListener valueUpdateListener = new ValueUpdateListener() {
        @Override
        public void onUpdate(AnimatorValue animatorValue, float value) {

            value = value * (end - start) + start;
            LogUtil.info("ValueUpdateListener", "start: " + start + " end: " + end + " value: " + value);
            if (myValueUpdateListener != null) {
                myValueUpdateListener.onUpdate(animatorValue, value);
            }
        }
    };
    /**
     * The constant M interpolator
     */
    private int mInterpolator = CurveType.DECELERATE;

    /**
     * Value animator
     */
    public ValueAnimator() {
        super();
    }

    /**
     * 获取一个自定义初始值和结束值的数值动画对象
     *
     * @param start 起始值
     * @param end   结束值
     * @return 自定义初始值和结束值的数值动画对象 value animator
     */
    public static ValueAnimator ofFloat(float start, float end) {
        ValueAnimator myValueAnimator = new ValueAnimator();
        myValueAnimator.setCurveType(CurveType.DECELERATE);
        myValueAnimator.start = start;
        myValueAnimator.end = end;
        return myValueAnimator;
    }

    /**
     * 获取一个自定义初始值和结束值的数值动画对象
     *
     * @return 自定义初始值和结束值的数值动画对象 value animator
     */
    public static ValueAnimator ofFloat() {
        ValueAnimator myValueAnimator = new ValueAnimator();
        return myValueAnimator;
    }

    /**
     * Get start float
     *
     * @return the float
     */
    public float getStart() {
        return start;
    }

    /**
     * Get end float
     *
     * @return the float
     */
    public float getEnd() {
        return end;
    }

    /**
     * 设置数值动画的起始值和结束值
     *
     * @param start 起始值
     * @param end   结束值
     */
    public void setFloatValues(float start, float end) {
        this.start = start;
        this.end = end;
    }

    /**
     * Set value update listener *
     *
     * @param listener listener
     */
    @Override
    public void setValueUpdateListener(ValueUpdateListener listener) {
        this.myValueUpdateListener = listener;
        super.setValueUpdateListener(valueUpdateListener);
    }

    /**
     * Set interpolator *
     *
     * @param mInterpolator m interpolator
     */
    public void setInterpolator(int mInterpolator) {
        this.mInterpolator = mInterpolator;
        setCurveType(mInterpolator);
    }
}
