/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.scwang.wave;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * Log util
 */
public class LogUtil {
    /**
     * * The constant TAG_LOG
     */
    private static final String TAG_LOG = "systembartint";

    /**
     * * The constant DOMAIN_ID
     */
    private static final int DOMAIN_ID = 0;

    /**
     * * The constant LABEL_LOG
     */
    private static final HiLogLabel LABEL_LOG = new HiLogLabel(3, DOMAIN_ID, LogUtil.TAG_LOG);

    /**
     * * The constant LOG_FORMAT
     */
    private static final String LOG_FORMAT = "%{public}s: %{public}s";

    /**
     * Log util
     */
    private LogUtil() {
        /* Do nothing */
    }

    /**
     * Debug *
     *
     * @param tag tag
     * @param msg msg
     */
    public static void debug(String tag, String msg) {
        HiLog.debug(LABEL_LOG, LOG_FORMAT, tag, msg);
    }

    /**
     * Info *
     *
     * @param tag tag
     * @param msg msg
     */
    public static void info(String tag, String msg) {
        HiLog.info(LABEL_LOG, LOG_FORMAT, tag, msg);
    }

    /**
     * Warn *
     *
     * @param tag tag
     * @param msg msg
     */
    public static void warn(String tag, String msg) {
        HiLog.warn(LABEL_LOG, LOG_FORMAT, tag, msg);
    }

    /**
     * Error *
     *
     * @param tag tag
     * @param msg msg
     */
    public static void error(String tag, String msg) {
        HiLog.error(LABEL_LOG, LOG_FORMAT, tag, msg);
    }

}
