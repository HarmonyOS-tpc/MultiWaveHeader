package com.scwang.wave;

import ohos.app.Context;

/**
 * Util
 */
public class Util {

    /**
     * 获取颜色
     *
     * @param context 上下文
     * @param colorId 颜色ID
     * @return 颜色 int
     */
    public static int getColor(Context context, int colorId) {
        return context.getColor(colorId);
    }

    /**
     * dp转px
     *
     * @param context context
     * @param dpVal   dp 值
     * @return px int
     */
    public static int dp2px(Context context, float dpVal) {
        return (int) (context.getResourceManager().getDeviceCapability().screenDensity / 160 * dpVal);
    }
}
